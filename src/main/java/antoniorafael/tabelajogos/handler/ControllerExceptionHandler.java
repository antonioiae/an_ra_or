package antoniorafael.tabelajogos.handler;

import antoniorafael.tabelajogos.services.exceptions.EquipeNaoEncontradaException;
import antoniorafael.tabelajogos.services.exceptions.PartidaNaoEncontradaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ControllerExceptionHandler {

    public static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(EquipeNaoEncontradaException.class)
    public ResponseEntity<?> handlerEquipeNaoEncontradaException(EquipeNaoEncontradaException e, HttpServletRequest request) {
        logger.error("Equipe não encontrada");
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(PartidaNaoEncontradaException.class)
    public ResponseEntity<?> handlerPartidaNaoEncontradaException(PartidaNaoEncontradaException e, HttpServletRequest request) {
        logger.error("Partida não encontrada");
        return ResponseEntity.notFound().build();
    }


}
