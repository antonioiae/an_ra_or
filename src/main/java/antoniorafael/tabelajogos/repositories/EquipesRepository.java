package antoniorafael.tabelajogos.repositories;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.EquipeVencedoraDTO;
import antoniorafael.tabelajogos.repositories.helpers.EquipesRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.List;

@Repository
public interface EquipesRepository extends JpaRepository<Equipe, Long>, EquipesRepositoryQueries {
    List<Equipe> findAllByOrderByGrupo();

    List<Equipe> findAllByGrupo(Integer grupo);

    Equipe findByGrupo(Integer grupo);

    @Query(nativeQuery = true,
            value = "SELECT p.grupo as grupo, p.id_equipevencedora as idEquipeVencedora, count( p.id_equipevencedora) as vitorias, isnull(r.rounds, 0) as rounds FROM app_partidas p LEFT JOIN (SELECT id_equipevencedora, count(1) as rounds FROM app_round GROUP BY id_equipevencedora) as r ON p.id_equipevencedora=r.id_equipevencedora GROUP BY p.id_equipevencedora ORDER BY p.grupo ASC,vitorias DESC, r.rounds DESC")
    List<Tuple> buscarEquipesVencedoras();

}
