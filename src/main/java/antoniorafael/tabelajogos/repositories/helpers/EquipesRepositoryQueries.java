package antoniorafael.tabelajogos.repositories.helpers;

import java.util.List;

public interface EquipesRepositoryQueries {
    public List<Integer> buscarGrupos();
}
