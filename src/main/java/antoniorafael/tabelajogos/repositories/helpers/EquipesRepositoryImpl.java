package antoniorafael.tabelajogos.repositories.helpers;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class EquipesRepositoryImpl implements EquipesRepositoryQueries {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Integer> buscarGrupos() {
        String jpql = "SELECT e.grupo FROM Equipe e GROUP BY e.grupo";
        return entityManager.createQuery(jpql, Integer.class).getResultList();
    }

}
