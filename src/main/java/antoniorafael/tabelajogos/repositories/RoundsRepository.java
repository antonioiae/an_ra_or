package antoniorafael.tabelajogos.repositories;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.Round;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoundsRepository extends JpaRepository<Round, Long> {
    Integer countByEquipeVencedora(Equipe equipe);
}
