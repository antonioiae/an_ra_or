package antoniorafael.tabelajogos.repositories;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.Partida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartidasRepository extends JpaRepository<Partida, Long> {
    List<Partida> findAllByGrupo(Integer grupo);

    List<Partida> findAllByEquipeVencedora(Equipe equipe);

    List<Partida> findAllByOrderByGrupo();
}
