package antoniorafael.tabelajogos.services;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.EquipeVencedoraDTO;
import antoniorafael.tabelajogos.repositories.EquipesRepository;
import antoniorafael.tabelajogos.services.exceptions.EquipeNaoEncontradaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class EquipesService {

    public static final Logger logger = LoggerFactory.getLogger(EquipesService.class);

    @Autowired
    private EquipesRepository equipesRepository;

    public Equipe buscar(Long id) {
        Optional<Equipe> equipe = equipesRepository.findById(id);
        if (!equipe.isPresent()) {
            throw new EquipeNaoEncontradaException("Equipe não encontrada");
        }
        return equipe.get();
    }

    public List<Equipe> listar() {
        return equipesRepository.findAll();
    }

    public Equipe salvar(Equipe equipe) {
        equipe.setId(null);
        return equipesRepository.save(equipe);
    }

    public Equipe atualizar(Equipe equipe) {
        verificarExistencia(equipe);
        return equipesRepository.save(equipe);
    }

    public void deletar(Long id) {
        try {
            equipesRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EquipeNaoEncontradaException("Equipe não encontrada");
        }
    }

    private void verificarExistencia(Equipe equipe) {
        buscar(equipe.getId());
    }

    public void limparEquipes() {
        logger.info("Zerando tabela de equipes");
        equipesRepository.deleteAll();
    }

    public List<Equipe> listarOrderGrupo() {
        return equipesRepository.findAllByOrderByGrupo();
    }

    public void gerarGrupos() {
        List<Equipe> equipes = this.listar();
        Collections.shuffle(equipes);

        int totalEquipes = equipes.size();
        int totalGrupos = (int) Math.floor(totalEquipes / 5);

        logger.info("Total de equipes: " + totalEquipes);
        logger.info("Total de grupos: " + totalGrupos);

        int count = 0;
        for (Equipe equipe : equipes) {
            int grupo = (count % totalGrupos) + 1;
            equipe.setGrupo(grupo);
            this.atualizar(equipe);
            count++;
        }
    }

    public List<Integer> buscarGrupos() {
        return equipesRepository.buscarGrupos();
    }

    public List<Equipe> buscarEquipesPeloGrupo(Integer grupo){
        return equipesRepository.findAllByGrupo(grupo);
    }

    public Equipe buscarEquipePeloGrupo(Integer grupo){
        return equipesRepository.findByGrupo(grupo);
    }

    public List<EquipeVencedoraDTO> listarEquipesVencedoras(){

        List<Tuple> tuplas = equipesRepository.buscarEquipesVencedoras();
        List<EquipeVencedoraDTO> equipesVencedora = new ArrayList<>();

        Integer grupoAnterior = 0;
        Integer itensGrupoAnterior = 0;
        Integer count = 0;

        for(Tuple tupla : tuplas){

            Integer grupo = tupla.get("grupo", Integer.class);
            BigInteger idEquipeVencedora = tupla.get("idEquipeVencedora", BigInteger.class);
            BigInteger vitorias = tupla.get("vitorias", BigInteger.class);
            BigInteger rounds = tupla.get("rounds", BigInteger.class);

            if(itensGrupoAnterior >= 2){
                boolean continuar = false;
                if(grupoAnterior != grupo){
                    itensGrupoAnterior = 0;
                    continuar = true;
                }
                grupoAnterior = grupo;
                if(!continuar) continue;
            }

            grupoAnterior = grupo;
            itensGrupoAnterior++;

            EquipeVencedoraDTO equipeVencedora = new EquipeVencedoraDTO(grupo, idEquipeVencedora, vitorias, rounds);
            if (idEquipeVencedora != null) {
                long idEquipe = idEquipeVencedora.longValue();
                Equipe equipe = buscar(idEquipe);
                equipeVencedora.setEquipe(equipe);
            }

            equipesVencedora.add(count,equipeVencedora);
            count++;
        }

        return equipesVencedora;
    }
}
