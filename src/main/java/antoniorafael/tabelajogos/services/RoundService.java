package antoniorafael.tabelajogos.services;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.Partida;
import antoniorafael.tabelajogos.domain.Round;
import antoniorafael.tabelajogos.repositories.RoundsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoundService {

    @Autowired
    private RoundsRepository roundsRepository;

    public Round salvar(Round round){
        round.setId(null);
        return roundsRepository.save(round);
    }


    public Equipe verificarGanhador(Partida partida) {

        Equipe primeiraEquipe = partida.getPrimeiraEquipe();
        Equipe segundaEquipe = partida.getSegundaEquipe();

        Integer numVitorias = roundsRepository.countByEquipeVencedora(primeiraEquipe);

        if(numVitorias >= 16){
            return primeiraEquipe;
        }else{
            numVitorias = roundsRepository.countByEquipeVencedora(segundaEquipe);
            if(numVitorias >= 16){
                return segundaEquipe;
            }
        }

        return null;

    }
}
