package antoniorafael.tabelajogos.services;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.EquipeVencedoraDTO;
import antoniorafael.tabelajogos.domain.Partida;
import antoniorafael.tabelajogos.domain.Round;
import antoniorafael.tabelajogos.repositories.PartidasRepository;
import antoniorafael.tabelajogos.services.exceptions.PartidaNaoEncontradaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class PartidasService {

    public static final Logger logger = LoggerFactory.getLogger(PartidasService.class);

    @Autowired
    private PartidasRepository partidasRepository;

    @Autowired
    private EquipesService equipesService;

    @Autowired
    private RoundService roundService;

    public List<Partida> listar(){
        return partidasRepository.findAllByOrderByGrupo();
    }

    public Partida salvar(Partida partida) {
        partida.setId(null);
        return partidasRepository.save(partida);
    }

    public Partida buscar(Long id) {
        Optional<Partida> partida = partidasRepository.findById(id);
        if (!partida.isPresent()) {
            throw new PartidaNaoEncontradaException("Partida não encontrada");
        }
        return partida.get();
    }

    public void gerarPartidas() {
        List<Integer> grupos = equipesService.buscarGrupos();
        logger.info(String.valueOf(grupos));

        for (Integer grupo : grupos) {
            List<Equipe> equipes = equipesService.buscarEquipesPeloGrupo(grupo);

            if (equipes.size() > 0) {

                int quantidade = (equipes.size() - 1);
                for (int i = 0; i <= quantidade; i++) {

                    for (int j = quantidade; j > 0; j--) {

                        if (j > i) {//não joga com ele mesmo

                            Partida partida = new Partida();
                            partida.setGrupo(grupo);
                            partida.setPrimeiraEquipe(equipes.get(i));
                            partida.setSegundaEquipe(equipes.get(j));
                            partidasRepository.saveAndFlush(partida);

                        }

                    }

                }

            }

        }

    }

    public void gerarResultados() {

        List<Partida> partidas = partidasRepository.findAllByOrderByGrupo();

        for(Partida partida : partidas){
            boolean loop = true;
            while (loop){//INUMEROS ROUNDS
                Round round = new Round();
                round.setPartida(partida);

                Random rand = new Random();
                int i = rand.nextInt(2) + 1;

                if(i == 1){
                    round.setEquipeVencedora(partida.getPrimeiraEquipe());
                }else{
                    round.setEquipeVencedora(partida.getSegundaEquipe());
                }
                roundService.salvar(round);

                Equipe equipeVencedora = roundService.verificarGanhador(partida);
                if(equipeVencedora != null){
                    partida.setEquipeVencedora(equipeVencedora);
                    loop = false;
                }
            }
        }
    }

    public void gerarPartidasPlayoffs() {

        List<EquipeVencedoraDTO> equipesVencedoraDTO = equipesService.listarEquipesVencedoras();

        for (Integer grupo : grupos) {
            List<Equipe> equipes = equipesService.buscarEquipesPeloGrupo(grupo);

            if (equipes.size() > 0) {

                int quantidade = (equipes.size() - 1);
                for (int i = 0; i <= quantidade; i++) {

                    for (int j = quantidade; j > 0; j--) {

                        if (j > i) {//não joga com ele mesmo

                            Partida partida = new Partida();
                            partida.setGrupo(grupo);
                            partida.setPrimeiraEquipe(equipes.get(i));
                            partida.setSegundaEquipe(equipes.get(j));
                            partidasRepository.saveAndFlush(partida);

                        }

                    }

                }

            }

        }
        
    }

}
