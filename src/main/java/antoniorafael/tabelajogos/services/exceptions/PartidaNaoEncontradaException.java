package antoniorafael.tabelajogos.services.exceptions;

public class PartidaNaoEncontradaException extends RuntimeException {

    public PartidaNaoEncontradaException(String mensagem) {
        super(mensagem);
    }

    public PartidaNaoEncontradaException(String mensagem, Throwable causa) {
        super(mensagem, causa);
    }
}
