package antoniorafael.tabelajogos.services.exceptions;

public class EquipeNaoEncontradaException extends RuntimeException {

    public EquipeNaoEncontradaException(String mensagem) {
        super(mensagem);
    }

    public EquipeNaoEncontradaException(String mensagem, Throwable causa){
        super(mensagem,causa);
    }

}
