package antoniorafael.tabelajogos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamersclubApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamersclubApplication.class, args);
	}

}
