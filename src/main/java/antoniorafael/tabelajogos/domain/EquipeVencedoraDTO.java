package antoniorafael.tabelajogos.domain;

import java.math.BigInteger;

public class EquipeVencedoraDTO {

    private Integer grupo;
    private BigInteger idEquipeVencedora;
    private BigInteger vitorias;
    private BigInteger rounds;
    private Equipe equipe;

    public EquipeVencedoraDTO() {

    }

    public EquipeVencedoraDTO(Integer grupo, BigInteger idEquipeVencedora, BigInteger vitorias, BigInteger rounds) {
        this.grupo = grupo;
        this.idEquipeVencedora = idEquipeVencedora;
        this.vitorias = vitorias;
        this.rounds = rounds;
    }

    public Integer getGrupo() {
        return grupo;
    }

    public void setGrupo(Integer grupo) {
        this.grupo = grupo;
    }

    public BigInteger getIdEquipeVencedora() {
        return idEquipeVencedora;
    }

    public void setIdEquipeVencedora(BigInteger idEquipeVencedora) {
        this.idEquipeVencedora = idEquipeVencedora;
    }

    public BigInteger getVitorias() {
        return vitorias;
    }

    public void setVitorias(BigInteger vitorias) {
        this.vitorias = vitorias;
    }

    public BigInteger getRounds() {
        return rounds;
    }

    public void setRounds(BigInteger rounds) {
        this.rounds = rounds;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    @Override
    public String toString() {
        return "EquipeVencedoraDTO{" + "grupo=" + grupo + ", idEquipeVencedora=" + idEquipeVencedora + ", vitorias=" + vitorias + ", rounds=" + rounds + '}';
    }
}
