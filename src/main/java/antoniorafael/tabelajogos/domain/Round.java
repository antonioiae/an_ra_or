package antoniorafael.tabelajogos.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "app_round")
public class Round {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer numeroRound;

    @ManyToOne
    @JoinColumn(name = "id_partida")
    @JsonIgnore
    private Partida partida;

    @ManyToOne
    @JoinColumn(name = "id_equipevencedora")
    @JsonIgnore
    private Equipe equipeVencedora;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumeroRound() {
        return numeroRound;
    }

    public void setNumeroRound(Integer numeroRound) {
        this.numeroRound = numeroRound;
    }

    public Partida getPartida() {
        return partida;
    }

    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    public Equipe getEquipeVencedora() {
        return equipeVencedora;
    }

    public void setEquipeVencedora(Equipe equipeVencedora) {
        this.equipeVencedora = equipeVencedora;
    }
}
