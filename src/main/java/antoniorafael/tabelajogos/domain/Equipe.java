package antoniorafael.tabelajogos.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "app_equipe")
public class Equipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "O nome da equipe não pode ser vazio")
    private String nome;

    private Integer grupo;

    @OneToMany(mappedBy = "equipeVencedora")
    @JsonIgnore
    private List<Partida> partidasVencidas;

    public Equipe() {
    }

    public Equipe(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getGrupo() {
        return grupo;
    }

    public void setGrupo(Integer grupo) {
        this.grupo = grupo;
    }

    public List<Partida> getPartidasVencidas() {
        return partidasVencidas;
    }

    public void setPartidasVencidas(List<Partida> partidasVencidas) {
        this.partidasVencidas = partidasVencidas;
    }

    @Override
    public String toString() {
        return nome;
    }
}
