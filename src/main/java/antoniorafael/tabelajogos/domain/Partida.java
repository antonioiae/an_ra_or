package antoniorafael.tabelajogos.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "app_partidas")
public class Partida {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_primeiraequipe")
    private Equipe primeiraEquipe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_segundaequipe")
    private Equipe segundaEquipe;

    @OneToMany(mappedBy = "partida")
    @JsonIgnore
    private List<Round> rounds;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_equipevencedora")
    @JsonIgnore
    private Equipe equipeVencedora;

    private Integer grupo;

    private Boolean playoff;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Equipe getEquipeVencedora() {
        return equipeVencedora;
    }

    public void setEquipeVencedora(Equipe equipeVencedora) {
        this.equipeVencedora = equipeVencedora;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public Equipe getPrimeiraEquipe() {
        return primeiraEquipe;
    }

    public void setPrimeiraEquipe(Equipe primeiraEquipe) {
        this.primeiraEquipe = primeiraEquipe;
    }

    public Equipe getSegundaEquipe() {
        return segundaEquipe;
    }

    public void setSegundaEquipe(Equipe segundaEquipe) {
        this.segundaEquipe = segundaEquipe;
    }

    public Integer getGrupo() {
        return grupo;
    }

    public void setGrupo(Integer grupo) {
        this.grupo = grupo;
    }

    public Boolean getPlayoff() {
        return playoff;
    }

    public void setPlayoff(Boolean playoff) {
        this.playoff = playoff;
    }
}
