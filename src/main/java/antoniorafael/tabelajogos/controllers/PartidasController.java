package antoniorafael.tabelajogos.controllers;

import antoniorafael.tabelajogos.domain.Partida;
import antoniorafael.tabelajogos.services.PartidasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/partidas")
public class PartidasController {

    @Autowired
    private PartidasService partidasService;

    @GetMapping
    public ModelAndView listar() {
        ModelAndView mv = new ModelAndView("partidas");
        mv.addObject("partidas",partidasService.listar());
        return mv;
    }

    public ModelAndView salvar(@RequestBody Partida partida){
        partidasService.salvar(partida);
        return listar();
    }

    @RequestMapping(value = "/gerar", method = RequestMethod.GET)
    public ModelAndView gerarPartidas() {
        partidasService.gerarPartidas();
        return new ModelAndView("redirect:/partidas");
    }

    @RequestMapping(value = "/resultados", method = RequestMethod.GET)
    public ModelAndView gerarResultados() {
        partidasService.gerarResultados();
        return new ModelAndView("redirect:/partidas");
    }

    public ModelAndView playoffs(){
        ModelAndView mv = new ModelAndView("playoffs");
        partidasService.gerarPartidasPlayoffs();
        return mv;
    }

}
