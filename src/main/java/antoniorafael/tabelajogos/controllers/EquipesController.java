package antoniorafael.tabelajogos.controllers;

import antoniorafael.tabelajogos.domain.Equipe;
import antoniorafael.tabelajogos.domain.EquipeVencedoraDTO;
import antoniorafael.tabelajogos.services.EquipesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.Tuple;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Controller
public class EquipesController {

    public static final Logger logger = LoggerFactory.getLogger(EquipesController.class);

    @Autowired
    private EquipesService equipesService;

    @RequestMapping(value = "/equipes", method = RequestMethod.GET)
    public ModelAndView listar() {
        ModelAndView mv = new ModelAndView("equipes");
        mv.addObject("equipes", equipesService.listar());
        return mv;
    }

    @RequestMapping(value = "/equipes/salvar", method = RequestMethod.POST)
    public ModelAndView salvar(@RequestBody Equipe equipe){
        equipesService.salvar(equipe);
        return listar();
    }

    @RequestMapping(value = "/equipes/{id}", method = RequestMethod.DELETE)
    public ModelAndView deletar(@PathVariable("id") Long id){
        equipesService.deletar(id);
        return listar();
    }

    @RequestMapping(value = "/equipes/gerar", method = RequestMethod.GET)
    public ModelAndView gerar() {
        equipesService.limparEquipes();
        logger.info("Cadastrando as 80 equipes");
        for (int i = 1; i <= 80; i++) {
            Equipe equipe = new Equipe("Equipe " + i);
            equipesService.salvar(equipe);
        }
        equipesService.gerarGrupos();
        return new ModelAndView("redirect:/equipes");
    }

    @RequestMapping(value = "/grupos", method = RequestMethod.GET)
    public ModelAndView listarGrupos() {
        ModelAndView mv = new ModelAndView("grupos");
        List<Equipe> equipes = equipesService.listarOrderGrupo();
        mv.addObject("equipes", equipes);
        mv.addObject("totalEquipes", equipes.size());
        mv.addObject("qtdGrupos", (int) Math.floor(equipes.size() / 5));
        return mv;
    }

    @RequestMapping(value = "/equipes/classificadas", method = RequestMethod.GET)
    public ModelAndView listarEquipesVencedoras(){
        List<EquipeVencedoraDTO> equipes = equipesService.listarEquipesVencedoras();
        logger.info(equipes.toString());
        ModelAndView mv = new ModelAndView("equipesVencedoras");
        mv.addObject("equipes",equipes);
        return mv;
    }

}
